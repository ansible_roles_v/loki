Promtail
=========
Install and configure Grafana Loki.  

Include role
------------
```yaml
- name: loki  
  src: https://gitlab.com/ansible_roles_v/loki/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: hosts
  gather_facts: true
  become: true
  roles:
    - loki
```