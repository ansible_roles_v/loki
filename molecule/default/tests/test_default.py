from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_files(host):
    files = [
        "/etc/systemd/system/loki.service",
        "/usr/local/bin/loki"
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file

def test_user(host):
    assert host.group("loki").exists
    assert "loki" in host.user("loki").groups
    assert host.user("loki").shell == "/usr/sbin/nologin"

def test_service(host):
    s = host.service("loki")
    assert s.is_enabled
    assert s.is_running

def test_socket(host):
    sockets = [
        "tcp://127.0.0.1:3100"
    ]
    for socket in sockets:
        s = host.socket(socket)
        assert s.is_listening
